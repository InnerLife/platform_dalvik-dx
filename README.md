# About
This is a fork of Android DX (Dalvik eXchange) tool and it is optimized for slow devices.
Splitted from official [repository](https://github.com/aosp-mirror/platform_dalvik).

Do you know DX tool has undocumented flag "set-max-idx-number" for many years? It works in pair with "multi-dex" flag and can be used to separate your huge jar to many small .dex files. This flags works well and can be already used on low-RAM devices, but... they dramatically decrease DX speed.
My fork is increasing DX speed a most for multi-dex and for no-multi-dex a little.

# Optimizations
It's a challenge to optimize already optimized code, but the following was done:

* Batch classes "translation" 
* Caching "translated" classes on disk 
* Removing unnecessary monitor locks to make all cores busy. 
* Caching some "hot" values(hash, max, etc.) 
* Replacing data structures, such as collections, with more effective ones. 

# How to use
Use it as ordinary DX tool, from command line or directly.
For example:
> dx --dex --no-optimize --no-strict --num-threads=4 --multi-dex --set-max-idx-number=2000 --output dexed.jar non-dexed.jar

or

> com.android.dx.command.Main.main(
                new String[]{"--dex", "--no-optimize", "--no-strict", "--num-threads=4",
                        "--multi-dex", "--set-max-idx-number=2000", "--output", "dexed.jar", "non-dexed.jar"});
                        
The lesser is set-max-idx-number, the lesser is RAM usage, but don't use extremely little values - NoClassDefFoundError may occurs.
                        
# Original DX Readme
Home of Dalvik eXchange, the thing that takes in class files and
reformulates them for consumption in the VM. It also does a few other
things; use "dx --help" to see a modicum of self-documentation.