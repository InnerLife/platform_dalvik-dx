package com.android.dx.command.dexer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by innerlife on 06.10.2017.
 */

public class LowMemoryPatch {

    private static final String TMP_CLASSES_PREFIX = "classes";
    private static final String TMP_CLASSES_EXTENSION = ".dex";

    private File tmpDir;

    /**
     * Construcotr
     * @param outName out file name
     */
    public LowMemoryPatch(String outName){
        tmpDir = new File(new File(outName).getParent(), UUID.randomUUID().toString());
        createTmpDirectory();
    }

    public synchronized FileByteProxy createFileByteProxy(byte[] bytes, int id){
        return new FileByteProxy(id, bytes);
    }

    public synchronized void removeFileByteProxies(){
        removeTmpDirectory();
    }

    private void createTmpDirectory(){
        removeTmpDirectory();
        tmpDir.mkdir();
    }

    private void removeTmpDirectory(){
        if (!tmpDir.exists()){
            return;
        }
        if(!tmpDir.isDirectory()){
            throw new RuntimeException(tmpDir.getAbsolutePath() + " is not a directory");
        }
        for(File file : tmpDir.listFiles()){
            file.delete();
        }
        tmpDir.delete();
    }

    private File getClassesFile(int id) {
        return new File(tmpDir.getAbsolutePath(), TMP_CLASSES_PREFIX + id + TMP_CLASSES_EXTENSION);
    }

    private static void writeFile(byte[] bytes, File file){
        //System.out.println("out " + file.getAbsolutePath());
        try(FileOutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write(bytes);
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    private static byte[] readFile(File file){
        //System.out.println("in " + file.getAbsolutePath());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        int len = 0;
        byte[] buffer = new byte[8000];
        try(FileInputStream inputStream = new FileInputStream(file)){
            while((len = inputStream.read(buffer)) > 0){
                outputStream.write(buffer, 0, len);
            }
        } catch (IOException e){
            throw new RuntimeException(e);
        }
        return outputStream.toByteArray();
    }

    public class FileByteProxy {
        private File file;
        private int id;

        private FileByteProxy(int id, byte[] bytes){
            this.file = getClassesFile(id);
            this.id = id;
            writeFile(bytes, file);
            bytes = null;
        }

        public byte[] getBytes(){
            return readFile(file);
        }

        public int getId() {
            return id;
        }
    }
}
